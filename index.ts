import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleComponent } from './src/sample.component';
import { SampleDirective } from './src/sample.directive';
import { SamplePipe } from './src/sample.pipe';
import { SampleService } from './src/sample.service';
import { MaterialModule } from '@angular/material';
import { RelatedPersonPictureComponent } from './src/related-person-picture/related-person-picture.component';

export * from './src/sample.component';
export * from './src/sample.directive';
export * from './src/sample.pipe';
export * from './src/sample.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    SampleComponent,
    SampleDirective,
    SamplePipe,
    RelatedPersonPictureComponent
  ],
  exports: [
    SampleComponent,
    SampleDirective,
    SamplePipe
  ]
})
export class RelatedPicturPerson {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RelatedPicturPerson,
      providers: [SampleService]
    };
  }
}
