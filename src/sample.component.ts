import { Component } from '@angular/core';

@Component({
  selector: 'sample-component',
  template: `<app-related-person-picture></app-related-person-picture>`
})
export class SampleComponent {

  constructor() {
  }

}
