import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedPersonPictureComponent } from './related-person-picture.component';

describe('RelatedPersonPictureComponent', () => {
  let component: RelatedPersonPictureComponent;
  let fixture: ComponentFixture<RelatedPersonPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedPersonPictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedPersonPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
